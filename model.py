import numpy as np


class Model(object):
    def __init__(self, a=0.2635, b=0.7655):
        self.a, self.b = a, b

    def predict(self, X):
        if not hasattr(X, "__len__"):
            if X <= self.a:
                return "operator"
            elif X <= self.b:
                return "reask"
            else:
                return "correct"

        y_pred = []
        for x in np.reshape(X, (-1)):
            if x <= self.a:
                y_pred.append("operator")
            elif x <= self.b:
                y_pred.append("reask")
            else:
                y_pred.append("correct")
        return np.array(y_pred)
